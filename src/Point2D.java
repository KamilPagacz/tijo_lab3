public class Point2D {
    private double posX, posY;

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public Point2D(double posX, double posY){
        this.posX = posX;
        this.posY = posY;
    }

    @Override
    public String toString(){
        return "x = "+posX+" y = "+posY;
    }

}
