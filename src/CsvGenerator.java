import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class CsvGenerator implements CSVGeneratorImpl {

    @Override
    public void toCsv(List<String> lines, String fileName) throws IOException {
        final String STARS = "*************";
        try{
            FileWriter csvWriter = new FileWriter(fileName);
            // pretty line
            printLine(csvWriter,STARS);
            // new line + '\n'
            for (String line : lines) {
                printLine(csvWriter,line);
            }
            // pretty line
            printLine(csvWriter,STARS);
            csvWriter.flush();
            csvWriter.close();
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printLine(FileWriter csvWriter, String line) throws IOException{
        csvWriter.append(line);
        csvWriter.append("\n");
    }

}