import java.io.IOException;
import java.util.List;

public interface CSVGeneratorImpl {
    void toCsv(List<String> lines, String fileName) throws IOException;
}
