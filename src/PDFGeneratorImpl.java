import java.util.List;

public interface PDFGeneratorImpl {
    void toPdf(List<String> lines);
}
