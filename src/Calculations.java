public class Calculations {


    public static Point2D positionGeometricCenter(Point2D [] inputPoints){

        double sumx = 0, sumy = 0;
        double posx = 0, posy = 0;

        for(Point2D point: inputPoints){
            sumx += point.getPosX();
            sumy += point.getPosY();
        }

        posx = sumx / inputPoints.length;
        posy = sumy / inputPoints.length;

        return new Point2D(posx,posy);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D [] inputPoints){

        //calculations
        double sumx = 0, sumy = 0, summ = 0;
        double centerx = 0, centery = 0;

        for(MaterialPoint2D point: inputPoints){
            sumx += point.getPosX() * point.getMass();
            sumy += point.getPosY() * point.getMass();
            summ += point.getMass();
        }

        centerx = sumx/summ;
        centery = sumy/summ;

        return new MaterialPoint2D(centerx, centery, summ);
    }
}
