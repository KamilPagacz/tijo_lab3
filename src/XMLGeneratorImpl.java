import java.util.List;

public interface XMLGeneratorImpl {
    void toXml(List<String> lines);
}
